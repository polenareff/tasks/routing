from math import sqrt, atan2

def scalar_product(x1, y1, x2, y2):
    return x1 * x2 + y1 * y2


def vector_product(x1, y1, x2, y2):
    return x1 * y2 - x2 * y1


def vec(x1, y1, x2, y2):
    return x2 - x1, y2 - y1


def dist(x1, y1, x2, y2):
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))


def point_inside_segment(x1, y1, x2, y2, x, y):
    return abs(dist(x, y, x1, y1) + dist(x, y, x2, y2) -
               dist(x1, y1, x2, y2)) <= 1e-7


def point_inside_field(field, x, y):
    angle_sum = 0
    for i in range(0, len(field)):
        j = (i + 1) % len(field)
        if point_inside_segment(field[i]['x'], field[i]['y'],
                                field[j]['x'], field[j]['y'], x, y):
            return True
        x1, y1 = vec(x, y, field[i]['x'], field[i]['y'])
        x2, y2 = vec(x, y, field[j]['x'], field[j]['y'])
        angle_sum += atan2(vector_product(x1, y1, x2, y2),
                           scalar_product(x1, y1, x2, y2))
    if abs(angle_sum) <= 1e-7:
        return False
    else:
        return True


def segments_intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    a1 = y1 - y2
    b1 = x2 - x1
    c1 = -a1 * x1 - b1 * y1
    a2 = y3 - y4
    b2 = x4 - x3
    c2 = -a2 * x3 - b2 * y3
    if abs(a1 * b2 - a2 * b1) <= 1e-7:
        return False
    x = (c2 * b1 - c1 * b2) / (a1 * b2 - a2 * b1)
    y = (a2 * c1 - a1 * c2) / (a1 * b2 - a2 * b1)
    if (point_inside_segment(x1, y1, x2, y2, x, y) and
            point_inside_segment(x3, y3, x4, y4, x, y)):
        return True
    else:
        return False


def check(field, x1, y1, x2, y2, x3, y3, x4, y4):
    for i in range(0, len(field)):
        j = (i + 1) % len(field)
        if segments_intersect(x1, y1, x2, y2, field[i]['x'], field[i]['y'],
                              field[j]['x'], field[j]['y']):
            return True
        if segments_intersect(x2, y2, x3, y3, field[i]['x'], field[i]['y'],
                              field[j]['x'], field[j]['y']):
            return True
        if segments_intersect(x3, y3, x4, y4, field[i]['x'], field[i]['y'],
                              field[j]['x'], field[j]['y']):
            return True
        if segments_intersect(x4, y4, x1, y1, field[i]['x'], field[i]['y'],
                              field[j]['x'], field[j]['y']):
            return True
    if point_inside_field(field, x1, y1):
        return True
    if point_inside_field([{'x': x1, 'y': y1}, {'x': x2, 'y': y2},
                           {'x': x3, 'y': y3}, {'x': x4, 'y': y4}],
                          field[0]['x'], field[0]['y']):
        return True


def count_route_dist(route):
    res = 0
    for i in range(1, len(route)):
        res += dist(route[i]['x'], route[i]['y'],
                    route[i - 1]['x'], route[i - 1]['y'])
    return res


def build_route(field, photo_points, drone):
    leftx = 1e9
    lefty = 1e9
    rightx = -1e9
    righty = -1e9
    upx = 1e9
    upy = -1e9
    downx = -1e9
    downy = 1e9
    for el in photo_points:
        if abs(el['x'] - leftx) <= 1e-7:
            if el['y'] < lefty:
                leftx = el['x']
                lefty = el['y']
        else:
            if el['x'] < leftx:
                leftx = el['x']
                lefty = el['y']
        if abs(el['x'] - rightx) <= 1e-7:
            if el['y'] > righty:
                rightx = el['x']
                righty = el['y']
        else:
            if el['x'] > rightx:
                rightx = el['x']
                righty = el['y']
        if abs(el['y'] - upy) <= 1e-7:
            if el['x'] < upx:
                upx = el['x']
                upy = el['y']
        else:
            if el['y'] > upy:
                upx = el['x']
                upy = el['y']
        if abs(el['y'] - downy) <= 1e-7:
            if el['x'] > downx:
                downx = el['x']
                downy = el['y']
        else:
            if el['y'] < downy:
                downx = el['x']
                downy = el['y']

    lux = upx
    luy = upy
    rux = rightx
    ruy = righty
    rdx = downx
    rdy = downy
    ldx = leftx
    ldy = lefty
    left_column = []
    right_column = []
    for el in photo_points:
        if point_inside_segment(lux, luy, ldx, ldy, el['x'], el['y']):
            left_column.append(el)
        if point_inside_segment(rux, ruy, rdx, rdy, el['x'], el['y']):
            right_column.append(el)
    left_column.sort(key=lambda coord: dist(lux, luy, coord['x'], coord['y']))
    right_column.sort(key=lambda coord: dist(rux, ruy, coord['x'], coord['y']))

    matrix = []
    for i in range(0, len(left_column)):
        row = []
        for el in photo_points:
            if point_inside_segment(left_column[i]['x'], left_column[i]['y'],
                                    right_column[i]['x'], right_column[i]['y'],
                                    el['x'], el['y']):
                row.append(el)
        row.sort(key=lambda coord: dist(left_column[i]['x'],
                                        left_column[i]['y'],
                                        coord['x'], coord['y']))
        matrix.append(row)

    drone_point = {'x': drone['x'], 'y': drone['y'],
                   'x1': 228, 'y1': 1337,
                   'x2': 228, 'y2': 1337,
                   'x3': 228, 'y3': 1337,
                   'x4': 228, 'y4': 1337,
                   'photo': False}
    route1 = [drone_point]
    route2 = [drone_point]
    route3 = [drone_point]
    route4 = [drone_point]

    for i in range(0, len(matrix)):
        if i % 2 == 0:
            for j in range(0, len(matrix[i])):
                route1.append(matrix[i][j])
            for j in range(len(matrix[i]) - 1, -1, -1):
                route2.append(matrix[i][j])
        else:
            for j in range(0, len(matrix[i])):
                route2.append(matrix[i][j])
            for j in range(len(matrix[i]) - 1, -1, -1):
                route1.append(matrix[i][j])
    route1.append(drone_point)
    route2.append(drone_point)

    for j in range(0, len(matrix[0])):
        if j % 2 == 0:
            for i in range(0, len(matrix)):
                route3.append(matrix[i][j])
            for i in range(len(matrix) - 1, -1, -1):
                route4.append(matrix[i][j])
        else:
            for i in range(0, len(matrix)):
                route4.append(matrix[i][j])
            for i in range(len(matrix) - 1, -1, -1):
                route3.append(matrix[i][j])
    route3.append(drone_point)
    route4.append(drone_point)

    route_dist = count_route_dist(route1)
    min_route = list(route1)
    if count_route_dist(route2) < route_dist:
        route_dist = count_route_dist(route2)
        min_route = list(route2)
    if count_route_dist(route3) < route_dist:
        route_dist = count_route_dist(route3)
        min_route = list(route3)
    if count_route_dist(route4) < route_dist:
        route_dist = count_route_dist(route4)
        min_route = list(route4)

    route = [drone_point]
    for el in min_route:
        if check(field, el['x1'], el['y1'], el['x2'], el['y2'],
                 el['x3'], el['y3'], el['x4'], el['y4']):
            route.append(el)
            route[-1]['photo'] = True
    route.append(drone_point)
    route_dist = count_route_dist(route)
    return {'stats': {'km_flew': route_dist,
                      'battery_min':
                          route_dist * drone['discharge_flight'] +
                          (len(route) - 2) * drone['discharge_photo']},
            'points': route}
