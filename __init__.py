from http.server import BaseHTTPRequestHandler, HTTPServer
import json

from .algo import build

class Server(BaseHTTPRequestHandler):

    def do_POST(self):
        content_length = int(self.headers.get('Content-Length'))
        request_json = self.rfile.read(content_length)
        try:
            request_data = json.loads(request_json)
            response_data = build(request_data)
            response_json = json.dumps(response_data)
        except Exception as e:
            print(e)
            self.send_response(400)
            self.end_headers()
        else:
            self.send_response(200)
            self.end_headers()
            self.wfile.write(response_json.encode())

httpd = HTTPServer(('0.0.0.0', 5000), Server)
httpd.serve_forever()

