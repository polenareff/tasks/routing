from numbers import Number
from math import sqrt, pi, atan2

def r2d(l):
    return l / pi * 180

def sort_points(res):
    v = (res[1][0] - res[0][0], res[1][1] - res[0][1])
    w = (res[2][0] - res[1][0], res[2][1] - res[1][1])
    l = min(abs(r2d(atan2(v[1], v[0]))), abs(180 - r2d(atan2(v[1], v[0]))))
    u = 90.0 - l
    a = res.copy()
    if (l < u):
        if (res[0][1] > res[3][1]):
            a[0] = res[3]
            a[1] = res[2]
            a[2] = res[1]
            a[3] = res[0]
        else:
            a[0] = res[1]
            a[1] = res[0]
            a[2] = res[3]
            a[3] = res[2]
    else:
        if (res[0][1] > res[1][1]):
            a[0] = res[2]
            a[1] = res[1]
            a[2] = res[0]
            a[3] = res[3]
        else:
            a[0] = res[0]
            a[1] = res[3]
            a[2] = res[2]
            a[3] = res[1]
    return a

def kos(a, b):
    return a[0] * b[1] - a[1] * b[0]

def minu(a, b):
    res = (a[0] - b[0], a[1] - b[1])
    return res

def convex_hull(points):
    v = sorted(points)
    n = len(points)
    st1 = []
    EPS = 0.0000001
    st1.append(0)
    st1.append(1)
    for i in range(2, n, 1):
        a = st1[len(st1) - 1];
        b = st1[len(st1) - 2];
        while (len(st1) > 1 and kos(minu(v[b], v[a]), minu(v[i], v[a])) <= EPS):
            st1.pop()
            if (len(st1) == 1):
                break
            a = st1[len(st1) - 1]
            b = st1[len(st1) - 2]
        st1.append(i)
    st1.pop()
    st2 = []
    st2.append(n - 1)
    st2.append(n - 2)
    for i in range(n - 3, -1, -1):
        a = st2[len(st2) - 1];
        b = st2[len(st2) - 2];
        while (len(st2) > 1 and kos(minu(v[b], v[a]), minu(v[i], v[a])) <= EPS):
            st2.pop();
            if (len(st2) == 1):
                break
            a = st2[len(st2) - 1];
            b = st2[len(st2) - 2];
        st2.append(i)
    st2.pop()
    ans = []
    for i in range(len(st1)):
        ans.append((v[st1[i]][0], v[st1[i]][1]))
    for i in range(len(st2)):
        ans.append((v[st2[i]][0], v[st2[i]][1]))
    return ans;

def dist(a, b):
    return sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

def convert_points(points):
    res = []
    for i in points:
        res.append((i['x'], i['y']))
    return res

def det(a, b, c, d):
    return a * d - b * c
 
def intersect(ma, mb, mc, na, nb, nc):
    zn = det(ma, mb, na, nb)
    res = (-det(mc, mb, nc, nb) / zn, -det(ma, mc, na, nc) / zn)
    return res

def in_seg(a, b, c):
    if (abs(dist(a, c) + dist(c, b) - dist(a, b)) < 0.000001):
        return True
    return False

def find_proj(a, b, p):
    la = a[1] - b[1]
    lb = b[0] - a[0]
    lc = (a[0] * b[1] - a[1] * b[0])

    v = (b[0] - a[0], b[1] - a[1])
    w = (-v[1], v[0])
    pp = (p[0] - w[0], p[1] - w[1])

    ua = p[1] - pp[1]
    ub = pp[0] - p[0]
    uc = (p[0] * pp[1] - p[1] * pp[0])

    inter = intersect(la, lb, lc, ua, ub, uc)
    return inter

def first_point(l, r, lx):
    v = (l[0] - r[0], l[1] - r[1])
    le = dist(l, r)
    v = (l[0] + v[0] / le * lx, l[1] + v[1] / le * lx)
    return v

def second_point(l, r, rx):
    v = (r[0] - l[0], r[1] - l[1])
    le = dist(l, r)
    v = (r[0] + v[0] / le * rx, r[1] + v[1] / le * rx)
    return v

def convert_res(r):
    res = []
    for i in r:
        res.append({'x': i[0], 'y': i[1]})
    return res


