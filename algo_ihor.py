from math import atan2, sqrt, ceil, exp
import random
import bisect

def get_dist(p1, p2):
	return sqrt((p1['x']-p2['x'])**2+(p1['y']-p2['y'])**2)

def convert_point(px, py, v1, v2, p0, w, h):
	nx = v1[0]*(1-py/h)+p0['x']
	ny = v1[1]*(1-py/h)+p0['y']			
	nx += v2[0]*px/w
	ny += v2[1]*px/w
	return (nx, ny)

def build_points(points, sw, sh, drone_x, drone_y, overlay):
	overlay = (100-overlay)/100
	w = get_dist(points[2], points[3])
	h = get_dist(points[1], points[2])				
	v2 = (points[3]['x']-points[2]['x'], points[3]['y']-points[2]['y'])
	v1 = (points[2]['x']-points[1]['x'], points[2]['y']-points[1]['y'])
	if sw>w and sh>h:
		res = []
		px = w/2
		py = h/2
		nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
		dct = {'x': nx, 'y': ny}
		px = w/2-sw/2
		py = h/2-sh/2
		nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
		dct['x1'] = nx
		dct['y1'] = ny
		px = w/2-sw/2
		py = h/2+sh/2
		nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
		dct['x2'] = nx
		dct['y2'] = ny
		px = w/2+sw/2
		py = h/2+sh/2
		nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
		dct['x3'] = nx
		dct['y3'] = ny
		px = w/2+sw/2
		py = h/2-sh/2
		nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
		dct['x4'] = nx
		dct['y4'] = ny
		res.append(dct)
		return res
	amt_w = ceil(w/(sw*overlay))
	amt_h = ceil(h/(sh*overlay))
	cur_left_w = 0
	cur_bot_h = 0
	res = []
	addFromZeroX = (drone_x<points[0]['x'] and drone_x<points[1]['x'] and drone_x<points[2]['x'] and drone_x<points[3]['x'])
	addFromZeroY = (drone_y<points[0]['y'] and drone_y<points[1]['y'] and drone_y<points[2]['y'] and drone_y<points[3]['y'])
	val_w_1 = sw*overlay
	val_w_2 = w-(w//(overlay*sw))*(sw*overlay)
	val_h_1 = sh*overlay
	val_h_2 = h-(h//(sh*overlay))*(sh*overlay)
	for i in range(amt_w):
		cur_bot_h = 0
		for j in range(amt_h):
			px = cur_left_w+sw/2
			py = cur_bot_h+sh/2
			nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
			dct = {'x': nx, 'y': ny}
			px = cur_left_w
			py = cur_bot_h
			nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
			dct['x1'] = nx
			dct['y1'] = ny		
			px = cur_left_w
			py = cur_bot_h+sh
			nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
			dct['x2'] = nx
			dct['y2'] = ny
			px = cur_left_w+sw
			py = cur_bot_h+sh
			nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
			dct['x3'] = nx
			dct['y3'] = ny
			px = cur_left_w+sw
			py = cur_bot_h
			nx, ny = convert_point(px, py, v1, v2, points[1], w, h)
			dct['x4'] = nx
			dct['y4'] = ny
			res.append(dct)
			if (addFromZeroY and j==0) or (addFromZeroY==False and j+1==amt_h-1):
				cur_bot_h+=val_h_2
			else:
				cur_bot_h+=val_h_1
		if (addFromZeroX and i==0) or (addFromZeroX==False and i+1==amt_w-1):
			cur_left_w+=val_w_2
		else:
			cur_left_w+=val_w_1
	return res

if __name__=='__main__':
	print(build_points([{'x':3, 'y':7}, {'x':4, 'y':3}, {'x':4, 'y':0}, {'x':7, 'y':4}], 1, 1, -100, -100))


