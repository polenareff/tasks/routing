if __name__=='__main__':
	from geom import *
else:
	from .geom import *

def check(a, b, c):
    v = (b[0] - a[0], b[1] - a[1])
    w = (c[0] - b[0], c[1] - b[1])
    EPS = 0.0000000001
    #assert(abs(v[0] * w[0] + v[1] * w[1]) < EPS)
    return None

def find_rectangle(points):
    converted_points = convert_points(points)
    hull = convex_hull(converted_points)
    n = len(hull)
    res = []
    min_s = 10 ** 10 * 1.0
    for i in range(4):
        res.append((0.0, 0.0))
    for i in range(n):
        j = (i + 1) % n
        l = hull[j]
        r = hull[i]
        lx = 0.0
        rx = 0.0
        ty = 0.0
        vec = (0.0, 0.0)
        for g in range(n):
            if (g == i or g == j):
                continue
            p = hull[g]
            pj = find_proj(l, r, p)
            if not in_seg(l, r, pj):
                if (dist(l, pj) > dist(r, pj)):
                    rx = max(rx, dist(r, pj))
                else:
                    lx = max(lx, dist(l, pj))
            if (ty < dist(p, pj)):
                ty = max(ty, dist(p, pj))
                vec = (p[0] - pj[0], p[1] - pj[1])
        s = (lx + dist(l, r) + rx) * ty
        if ty > 20000 or lx + dist(l, r) + rx > 20000:
            assert(False)
        if (s < min_s):
            min_s = s
            res[0] = first_point(l, r, lx)
            res[1] = second_point(l, r, rx)
            res[2] = (res[1][0] + vec[0], res[1][1] + vec[1]) 
            res[3] = (res[0][0] + vec[0], res[0][1] + vec[1])
    s_res = sort_points(res)
    check(s_res[0], s_res[1], s_res[2])
    return convert_res(s_res)


if __name__ == "__main__":
    points = [{'x': 2545962.6195633854, 'y': 5581679.465511303}, {'x': 2546210.213119177, 'y': 5581268.002800022}, {'x': 2546628.9375150073, 'y': 5581741.364230108}, {'x': 2546381.343959213, 'y': 5582156.4311665455}]
    print(find_rectangle(points))
