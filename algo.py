from .algo_andrii import build_route
from .algo_ihor import build_points
from .algo_denys import find_rectangle
from math import floor, cos, acos, ceil
INF = 1000000000
ymid = 0

def deg_to_rad(deg):
        return deg*acos(-1)/180

def rad_to_deg(rad):
        return rad*180/acos(-1)

def lat_lng_to_meters(field):
        res = []
        ymin = INF
        ymax = -INF
        for i in range(len(field)):
                point = field[i]
                ymin = min(ymin, point['y'])
                ymax = max(ymax, point['y'])
                print(point['y'])
        global ymid
        ymid = deg_to_rad((ymin+ymax)/2)
        for i in range(len(field)):
                point = field[i]
                new_point = {'x': 1, 'y': 1}
                new_point['x'] = point['x']*40075000/360*cos(ymid)
                new_point['y'] = point['y']*111320
                res.append(new_point)
        return res
def meters_to_lat_lng(new_points):
        res = []
        for i in range(len(new_points)):
                point = new_points[i]
                old_point = {}
                old_point['y'] = point['y']/111320
                old_point['y1'] = point['y1']/111320
                old_point['y2'] = point['y2']/111320
                old_point['y3'] = point['y3']/111320
                old_point['y4'] = point['y4']/111320

                old_point['x'] = point['x']*360/40075000/cos(ymid)
                old_point['x1'] = point['x1']*360/40075000/cos(ymid)
                old_point['x2'] = point['x2']*360/40075000/cos(ymid)
                old_point['x3'] = point['x3']*360/40075000/cos(ymid)
                old_point['x4'] = point['x4']*360/40075000/cos(ymid)
        
                old_point['photo'] = point['photo']
                res.append(old_point)
        return res

def build(request_data):
        field = request_data['field']
        drone = request_data['drone']
        dct_list = [{'x': drone['x'], 'y': drone['y']}]
        dct_list = lat_lng_to_meters(dct_list)
        drone['x'] = dct_list[0]['x']
        drone['y'] = dct_list[0]['y']
        drone_charge = drone['charge']
        field = lat_lng_to_meters(field)
        rect = find_rectangle(field)
        photo_points = build_points(rect, drone['photo_w'], drone['photo_h'], drone['x'], drone['y'], drone['photo_overlay'])        
        if drone['many_packs']==False:
                result = build_route(field, photo_points, drone)
                result['points'] = meters_to_lat_lng(result['points'])
                result['stats']['optimal_height'] = drone['max_height']
                result['stats']['battery_packs'] = ceil(result['stats']['battery_min']/drone_charge)
                result['stats']['km_flew']/=1000
                return result
        else:
                l = 50
                r = drone['max_height']
                m_height = 0
                
                max_charge = drone['packs_amount'] * drone['charge']
                for _ in range(20):
                        m_height = (r+l)//2
                        print(m_height)
                        K = m_height/drone['max_height'] 
                        photo_points = build_points(rect, drone['photo_w']*K, drone['photo_h']*K, drone['x'], drone['y'], drone['photo_overlay'])        
                        drone_temp = drone.copy()
                        result = build_route(field, photo_points, drone_temp)
                        if result['stats']['battery_min']>max_charge:
                                l = m_height
                        else:
                                r = m_height
                
                K = m_height/drone['max_height']
                
                photo_points = build_points(rect, drone['photo_w']*K, drone['photo_h']*K, drone['x'], drone['y'], drone['photo_overlay'])        
                drone_temp = drone.copy()
                result = build_route(field, photo_points, drone_temp)
                result['points'] = meters_to_lat_lng(result['points'])
                result['stats']['optimal_height'] = m_height
                result['stats']['battery_packs'] = drone['packs_amount']
                result['stats']['km_flew']/=1000
                return result

